const ZinkyCRUD = require("zinky-crud");

const crypto = require("crypto");
const algorithm = "aes-256-cbc";

class ZinkySign extends ZinkyCRUD {

  get loginOnRegister() { return true; }

  async POST_register(req, res) {
    const user = await this.POST_root(req, res);
    if (this.loginOnRegister) return this.createSession(user);
    res.deliver(204);
  }

  transformPwd(pwd) {
    const hash = crypto.createHash("sha256");
    hash.update(pwd);
    return hash.digest("hex");
  }

  defaultMatcher(req) {
    return {$match:{
      email: req.body.email,
      password: this.transformPwd(req.body.password),
      trash: { $ne: true },
    }};
  }

  async aggregation(req) {
    return [ this.defaultFilter(req) ]
  }

  async POST_login(req) {
    if (!req.body.email || !req.body.password)
      throw { statusCode: 400, msg: "Missing Credentials" };
    const [user] = await this.model.aggregate(await this.aggregation(req));
    if (!user) throw { statusCode: 401, msg: "Invalid Credentials" };
    const session = this.createSession(user);
    return session;
  }

  createToken(user) {
    return {...user};
  }

  createSession(user) {
    const token = this.createToken(user);
    return { token: this.encrypt(JSON.stringify(token)) };
  }

  requestToken(req) {
    return req.headers.token
  }

  async loadByToken(token) {
    return token;
  }

  async GET_me(req) {
    try {
      const token = JSON.parse(this.decrypt(this.requestToken(req)))
      const user = await this.loadByToken(token, req)
      return user;
    } catch (e) {
      console.error(e)
      return {};
    }
  }

  async loadSession(req) {
    req.user = req.headers.token ? await this.GET_me(req) : {};
  }

  encrypt(text) {
    let iv = crypto.randomBytes(16);
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(this.secretKey), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return `${iv.toString("hex")}:${encrypted.toString("hex")}`;
  }

  decrypt(text) {
    let textParts = text.split(":");
    let iv = new Buffer(textParts.shift(), "hex");
    let encryptedText = new Buffer(textParts.join(":"), "hex");
    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(this.secretKey), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
  }


}

module.exports = ZinkySign;
