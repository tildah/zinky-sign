const Zongel = require("zongel");

class ZongelSign extends Zongel {

  constructor(md) {
    super(md);
    this.ajv.addType("password", {
      compile: () => {
        return (data, path, object, key) => {
          object[key] = md.transformPwd(data);
          return true;
        }
      }
    });
    this.ajv.addType("email", {
      compile: () => {
        return (data, path, object, key) => {
          const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/;
          object[key] = data.toLowerCase();
          return re.test(object[key]);
        }
      }
    });
  }

}

module.exports = ZongelSign;
